/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2013,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.2
 * @date       2014-10-09
 */

#include "common.h"
#include "include.h"


/*!
 *  @brief      main函数
 *  @since      v5.2
 *  @note       测试查询接收多个字符串函数
 */
void main()
{
    char str[100];

    //uart_init(UART3,115200);     //初始化串口(由于 printf 函数 所用的端口就是 UART3，已经初始化了，因此此处不需要再初始化)

    printf("\n山外论坛:www.vcan123.com");

    uart_putstr    (UART3 , "\n\n\n请上位机发送多个字符:");             //发送字符串

    while(1)
    {
        if(uart_querystr (UART3,str,sizeof(str)-1) != 0)                //查询是否接收到字符串
        {
            uart_putstr (UART3, "\n接收到字符串：");                    //发送字符串
            uart_putstr (UART3, (uint8 *)str);                          //发送字符串
        }
    }
}

