/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2013,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.0
 * @date       2013-08-28
 */

#include "common.h"
#include "include.h"

void PORTE_IRQHandler();

/*! 
 *  @brief      main函数
 *  @since      v5.0
 *  @note       SPI 驱动 NRF24L01+ 
 */
void main(void)
{
    uint8 buff[DATA_PACKET];                        //定义接收缓冲区
    uint8 relen;

    printf("\n\n\n***********无线模块NRF24L01+测试************");

    while(!nrf_init())        //初始化NRF24L01+ ,等待初始化成功为止
    {
        printf("\n  NRF与MCU连接失败，请重新检查接线。\n");
    }
    //配置中断服务函数
    set_vector_handler(PORTE_VECTORn ,PORTE_IRQHandler);                //设置 PORTE 的中断服务函数为 PORTE_VECTORn
    enable_irq(PORTE_IRQn);

    printf("\n      NRF与MCU连接成功！\n"); 

    while(1)
    {            
        relen = nrf_rx(buff,DATA_PACKET);               //等待接收一个数据包，数据存储在buff里
        if(relen != 0)
        {
            printf("\n接收到数据:%s",buff);             //打印接收到的数据
        }
    }
}

/*!
 *  @brief      PORTE中断服务函数
 *  @since      v5.0
 */
void PORTE_IRQHandler()
{
    uint8  n;    //引脚号
    uint32 flag;

    flag = PORTE_ISFR;
    PORTE_ISFR  = ~0;                                   //清中断标志位

    n = 27;
    if(flag & (1 << n))                                 //PTE27触发中断
    {
        nrf_handler();
    }
}

