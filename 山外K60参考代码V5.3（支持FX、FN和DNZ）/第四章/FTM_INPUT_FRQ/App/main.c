/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2013,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.0
 * @date       2013-08-28
 */

#include "common.h"
#include "include.h"
#if  0
//全局变量定义
volatile uint32 cnvtime = 0;                //输入捕捉值

//函数声明
void FTM0_INPUT_IRQHandler(void);        //FTM0中断服务函数

/*!
 *  @brief      main函数
 *  @since      v5.0
 *  @note       FTM 输入捕捉 测脉冲宽度 测试
 */
void main()
{
    ftm_input_init(FTM0, FTM_CH6, FTM_Falling,FTM_PS_2);     //初始化FTM输入捕捉模式，下降沿捕捉(FTM0_CH6 为 PTD6)
    set_vector_handler(FTM0_VECTORn ,FTM0_INPUT_IRQHandler);    //设置FTM0的中断服务函数为 FTM0_INPUT_IRQHandler
    enable_irq (FTM0_IRQn);                                     //使能FTM0中断

    ftm_pwm_init(FTM1, FTM_CH1,10*1000,50);

    while(1)
    {
        if(cnvtime != 0)
        {
            //捕捉频率 = bus 时钟 / (分频系数) / cnvtime
            //分频系数 是 由 初始化时传递进去的分频因子FTM_PS_e 决定，分频系数 = 1<<FTM_PS_e
            //最大支持频率为 bus 时钟 四分之一
            printf("\n捕捉到频率为：%d",bus_clk_khz*1000/(1<<FTM_PS_2)/cnvtime);
            cnvtime = 0;
        }
        else
        {
            printf("\n没有捕捉到频率");
        }

        DELAY_MS(500);      //这里 的延时 ，仅仅是 避免 过于频繁打印数据到串口
    }
}

void FTM0_INPUT_IRQHandler(void)
{
    uint8 s = FTM0_STATUS;          //读取捕捉和比较状态  All CHnF bits can be checked using only one read of STATUS.
    uint8 CHn;

    FTM0_STATUS = 0x00;             //清中断标志位    All CHnF bits can be cleared by reading STATUS followed by writing 0x00 to STATUS.

    CHn = 6;
    if( s & (1 << CHn) )
    {
        /*     用户任务       */
        cnvtime = ftm_input_get(FTM0,(FTM_CHn_e)CHn);     //保存
        ftm_input_clean(FTM0);                 //清 计数器计数值

        /*********************/
    }
}


#else
//全局变量定义
volatile uint32 cnvtime = 0;                //输入捕捉值

//函数声明
void FTM0_INPUT_IRQHandler(void);        //FTM0中断服务函数

/*!
 *  @brief      main函数
 *  @since      v5.2
 *  @note       FTM 输入捕捉 测试
 */
void main()
{
    ftm_input_init(FTM0, FTM_CH6, FTM_Falling,FTM_PS_2);     //初始化FTM输入捕捉模式，下降沿捕捉(FTM0_CH6 为 PTD6)
    set_vector_handler(FTM0_VECTORn ,FTM0_INPUT_IRQHandler);    //设置FTM0的中断服务函数为 FTM0_INPUT_IRQHandler
    enable_irq (FTM0_IRQn);                                     //使能FTM0中断

    ftm_pwm_init(FTM1, FTM_CH1,10*1000,50);

    while(1)
    {
        if(cnvtime != 0)
        {
            //捕捉频率 = bus 时钟 / (分频系数) / cnvtime
            //分频系数 是 由 初始化时传递进去的分频因子FTM_PS_e 决定，分频系数 = 1<<FTM_PS_e
            //最大支持频率为 bus 时钟 四分之一

            printf("\n捕捉到频率为：%d",bus_clk_khz*1000/(1<<FTM_PS_2)/cnvtime);

            DELAY_MS(100);

            //等需要采集脉冲的时候，就开中断，过一段时间就可以获取到相应的值
            cnvtime = 0;

            FTM0_STATUS = 0x00;            //清中断标志位
            FTM_IRQ_EN(FTM0,FTM_CH0);

        }
        else
        {
            printf("\n没有捕捉到频率");
        }

        DELAY_MS(500);      //这里 的延时 ，仅仅是 避免 过于频繁打印数据到串口
    }
}


void FTM0_INPUT_IRQHandler(void)
{
    static uint8 flag = 0;
    uint8 s = FTM0_STATUS;          //读取捕捉和比较状态  All CHnF bits can be checked using only one read of STATUS.
    uint8 CHn;

    FTM0_STATUS = 0x00;

    CHn = 6;
    if( s & (1 << CHn) )
    {
        /*     用户任务       */
        if(flag == 0)
        {
            //第一次进来，开始计时
            flag++;
            ftm_input_clean(FTM0);            //清 计数器计数值
        }
        else if(flag == 1)
        {
            //第二次测到整个周期的时间
            cnvtime = ftm_input_get(FTM0, FTM_CH6); //保存
            flag = 0;

            FTM_IRQ_DIS(FTM0,FTM_CH0);      //关闭FTM0_CH0 中断  （避免频繁中断）
        }
        /*********************/
    }
}

#endif