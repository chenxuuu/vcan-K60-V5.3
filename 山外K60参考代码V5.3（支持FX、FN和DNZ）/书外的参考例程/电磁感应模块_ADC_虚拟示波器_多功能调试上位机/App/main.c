/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2015,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.3
 * @date       2015-04-07
 */

#include "common.h"
#include "include.h"


#define AMP1     ADC0_SE8        //PTB0
#define AMP2     ADC0_SE9        //PTB1
#define AMP3     ADC0_SE12       //PTB2
#define AMP4     ADC0_SE13       //PTB3
#define AMP5     ADC1_SE10       //PTB4
#define AMP6     ADC1_SE11       //PTB5



void main()
{
    //上位机 虚拟示波器需要配置成 波形数为6，数据类型为 uint8_t

    uint8 var[6];

    adc_init(AMP1);
    adc_init(AMP2);
    adc_init(AMP3);
    adc_init(AMP4);
    adc_init(AMP5);
    adc_init(AMP6);

    while(1)
    {
        var[0] = adc_once   (AMP1, ADC_8bit);
        var[1] = adc_once   (AMP2, ADC_8bit);
        var[2] = adc_once   (AMP3, ADC_8bit);
        var[3] = adc_once   (AMP4, ADC_8bit);
        var[4] = adc_once   (AMP5, ADC_8bit);
        var[5] = adc_once   (AMP6, ADC_8bit);

        vcan_sendware(var, sizeof(var));
        DELAY_MS(10);
    }

}

