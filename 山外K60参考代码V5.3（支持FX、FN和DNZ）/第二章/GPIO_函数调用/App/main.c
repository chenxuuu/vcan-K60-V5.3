/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2013,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.2
 * @date       2014-10-04
 */

#include "common.h"
#include "include.h"

/*!
 *  @brief      main函数
 *  @since      v5.2
 *  @note       GPIO简单函数调用实验

                山外K60核心板， PTB20对应于 LED0，低电平亮，高电平灭

                实验效果：LED0亮灭闪烁
 */
void main()
{
    gpio_init(PTB20,GPO,1);                         //初始化LED0  ，灭
    while(1)
    {
        gpio_set(PTB20,0);                          //PTB20输出0，即LED0 亮
        DELAY_MS(500);                              //延时500ms
        gpio_set(PTB20,1);                          //PTB20输出1，即LED0 灭
        DELAY_MS(500);                              //延时500ms
    }
}

