/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2013,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.2
 * @date       2014-10-04
 */

#include "common.h"
#include "include.h"

/*!
 *  @brief      main函数
 *  @since      v5.2
 *  @note       51编程风格的GPIO实验输入输出测试

                山外K60核心板， PTB20对应于 LED0，低电平亮，高电平灭
                                PTD7对应于独立按键，需要内部上拉，按下时为低电平

                实验效果：独立按键按下时，LED0亮；独立按键弹起时，LED0灭
 */
void main()
{
    gpio_init(PTB20,GPO,1);                     //初始化LED0  ，灭

    gpio_init(PTD7,GPI,0);                      //初始化核心板独立按键，内部上拉电阻
    port_init_NoALT(PTD7,PULLUP);

    while(1)
    {
        if(PTD7_IN == 1)                        //按键弹起
        {
            PTB20_OUT   = 1;                    //PTB20输出1，即LED0 灭
        }
        else                                    //按键按下
        {
            PTB20_OUT   = 0;                    //PTB20输出0，即LED0 亮
        }
        //实际上while循环可简化为：PTB20_OUT = PTD7_IN;
    }
}


