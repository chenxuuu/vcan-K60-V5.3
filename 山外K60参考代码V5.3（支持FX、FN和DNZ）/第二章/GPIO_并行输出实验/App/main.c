/*!
 *     COPYRIGHT NOTICE
 *     Copyright (c) 2013,山外科技
 *     All rights reserved.
 *     技术讨论：山外论坛 http://www.vcan123.com
 *
 *     除注明出处外，以下所有内容版权均属山外科技所有，未经允许，不得用于商业用途，
 *     修改内容时必须保留山外科技的版权声明。
 *
 * @file       main.c
 * @brief      山外K60 平台主程序
 * @author     山外科技
 * @version    v5.2
 * @date       2014-10-04
 */

#include "common.h"
#include "include.h"

/*!
 *  @brief      main函数
 *  @since      v5.2
 *  @note       GPIO并行输出实验

                山外K60核心板， PTB20~PTB23对应于 LED0~LED3，低电平亮，高电平灭

                实验效果：LED0/LED3同时亮灭，LED1/LED2同时亮灭，4则分别闪烁
 */
void main()
{
    //IO口需要一个个初始化
    gpio_init(PTB20,GPO,1);                         //初始化LED0  ，灭
    gpio_init(PTB21,GPO,1);                         //初始化LED1  ，灭
    gpio_init(PTB22,GPO,1);                         //初始化LED2  ，灭
    gpio_init(PTB23,GPO,1);                         //初始化LED3  ，灭

    while(1)
    {
        GPIO_SET_NBIT(4,PTB20,9);                   //PTB20/PTB23输出1，即LED0/LED3 灭;PTB21/PTB22输出0，即LED1/LED2 亮
        DELAY_MS(500);                              //延时500ms
        GPIO_SET_NBIT(4,PTB20,6);                   //PTB20/PTB23输出0，即LED0/LED3 亮;PTB21/PTB22输出1，即LED1/LED2 灭
        DELAY_MS(500);                              //延时500ms
    }
}

